<?php

/**
 * @file
 * Provides rules hooks for commerce_ticket.
 */

function commerce_ticket_rules_action_info() {
  $actions = array();

  $actions['commerce_ticket_update_line_item_ticket_status'] = array(
    'label' => t('Set the ticket status for line item tickets'),
    'description' => t('Sets the ticket status for tickets belonging to a line item'),
    'group' => t('Commerce Ticket'),
    'parameter' => array(
      'line_item_wrapper' => array(
        'type' => 'commerce_line_item',
        'label' => t('Commerce Line Item'),
        'wrapped' => TRUE,
      ),
      'state_wrapper' => array(
        'type' => 'ticket_state',
        'label' => t('Ticket state'),
        'wrapped' => TRUE,
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_ticket_update_line_item_ticket_status',
    ),
  );

  $actions['commerce_ticket_create_or_fetch_existing_cart'] = array(
    'label' => t('Create or fetch current cart'),
    'description' => t('Creates a new cart or loads and existing one.'),
    'group' => t('Commerce Ticket'),
    'provides' => array(
      'order' => array(
        'label' => t('Order'),
        'type' => 'commerce_order',
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_ticket_create_or_fetch_existing_cart',
    ),
  );

  // Define rules action to load selected product reference values from
  // a ticket registration.
  $actions['commerce_ticket_load_product_references_from_ticket'] = array(
    'label' => t('Get products from ticket registration'),
    'description' => t('Loads any selected product reference values from a ticket registration'),
    'group' => t('Commerce Ticket'),
    'parameter' => array(
      'ticket_registration' => array(
        'type' => 'ticket_registration',
        'label' => t('Ticket Registration'),
        'wrapped' => TRUE,
      )
    ),
    'provides' => array(
      'products' => array(
        'type' => 'list<commerce_product>',
        'label' => t('Products'),
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_ticket_load_product_references_from_ticket'
    ),
  );

  $actions['commerce_ticket_load_related_tickets_from_order'] = array(
    'label' => t('Load related tickets from an order'),
    'group' => t('Commerce Ticket'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Commerce Order'),
        'wrapped' => TRUE,
      )
    ),
    'provides' => array(
      'tickets' => array(
        'type' => 'list<ticket_registration>',
        'label' => t('Tickets'),
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_ticket_load_related_tickets_from_order'
    ),
  );

  return $actions;
}

/**
 * Rules action callback to set the ticket state of a line item.
 *
 * @param $line_item object
 *   The commerce line item object.
 * @param $state object
 *   The ticket state entity object.
 */
function commerce_ticket_update_line_item_ticket_status($line_item_wrapper, $state_wrapper) {

  // Make sure the current line item has a ticket reference field.
  if (!empty($line_item_wrapper->commerce_ticket_ticket)) {
    try {
      // Access the ticket reference through the wrapper.
      $ticket_wrapper = $line_item_wrapper->commerce_ticket_ticket;
      $ticket = $ticket_wrapper->value();

      // If we have a valid ticket reference, update the state.
      if (!empty($ticket)) {
        $ticket_wrapper->state->set($state_wrapper->name->value());
        $ticket_wrapper->save();
      }
    } catch (Exception $ex) {
      watchdog('commerce_ticket', '@error', array('@error' => $ex->getMessage()));
    }
  }
}

/**
 * Rules callback to create or fetch an existing cart.
 */
function commerce_ticket_create_or_fetch_existing_cart() {
  global $user;

  $order = commerce_cart_order_load($user->uid);

  if (!empty($order)) {
    return array('order' => $order);
  }

  $order =  commerce_cart_order_new($user->uid);
  return array('order' => $order);
}

/**
 * Rules callback to load product reference values from a ticket.
 */
function commerce_ticket_load_product_references_from_ticket($ticket_registration_wrapper) {
  $products = array();
  foreach ($ticket_registration_wrapper->getPropertyInfo() as $property_name => $property) {
    if ($property['type'] == 'commerce_product') {
      $products[] = $ticket_registration_wrapper->{$property_name}->value();
    }
    if ($property['type'] == 'list<commerce_product>') {
      $products = array_merge($products, (array) $ticket_registration_wrapper->{$property_name}->value());
    }
  }
  return array('products' => $products);
}

/**
 * Rules action callback to fetch ticket registrations from an oroder.
 */
function commerce_ticket_load_related_tickets_from_order($order_wrapper) {
  $tickets = array();

  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    if ($line_item_wrapper->type->value() == 'commerce_ticket') {
      try {
        $tickets[] = $line_item_wrapper->commerce_ticket_ticket->value();
      }
      catch (Exception $ex) {}
    }
  }

  return array('tickets' => $tickets);
}
