<?php

/**
 * Provides default rules for commerce_ticket.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_ticket_default_rules_configuration() {
  // Import the rule that creates a line item when a new ticket
  // is registered.
  $rule = '
    { "rules_add_ticket_products_to_cart_when_a_ticket_is_registered" : {
        "LABEL" : "Add ticket products to cart when a ticket is registered",
        "PLUGIN" : "reaction rule",
        "OWNER" : "rules",
        "REQUIRES" : [ "commerce_ticket", "rules", "ticket" ],
        "ON" : { "ticket_registration_insert" : [] },
        "DO" : [
          { "commerce_ticket_create_or_fetch_existing_cart" : { "PROVIDE" : { "order" : { "order" : "Order" } } } },
          { "entity_fetch" : {
              "USING" : {
                "type" : "ticket_type",
                "id" : [ "ticket-registration:bundle" ],
                "revision_id" : [ "" ]
              },
              "PROVIDE" : { "entity_fetched" : { "ticket_type" : "Ticket Type" } }
            }
          },
          { "entity_create" : {
              "USING" : {
                "type" : "commerce_line_item",
                "param_order" : [ "order" ],
                "param_type" : "commerce_ticket",
                "param_line_item_label" : "Ticket Registration",
                "param_quantity" : "1",
                "param_commerce_unit_price" : { "value" : { "amount" : 0, "currency_code" : "USD" } },
                "param_commerce_total" : { "value" : { "amount" : 0, "currency_code" : "USD" } }
              },
              "PROVIDE" : { "entity_created" : { "ticket_line_item" : "Ticket Line Item" } }
            }
          },
          { "data_set" : { "data" : [ "ticket-line-item:type" ], "value" : "commerce_ticket" } },
          { "data_set" : {
              "data" : [ "ticket-line-item:commerce-ticket-ticket" ],
              "value" : [ "ticket-registration" ]
            }
          },
          { "data_set" : {
              "data" : [ "ticket-line-item:commerce-product" ],
              "value" : [ "ticket-type:commerce-ticket-product" ]
            }
          },
          { "data_set" : {
              "data" : [ "ticket-line-item:commerce-display-path" ],
              "value" : "node\/[ticket-type:entity-id]"
            }
          },
          { "entity_save" : { "data" : [ "ticket-line-item" ], "immediate" : "1" } },
          { "list_add" : {
              "list" : [ "order:commerce-line-items" ],
              "item" : [ "ticket-line-item" ],
              "unique" : "1"
            }
          },
          { "drupal_message" : { "message" : "[ticket-type:commerce-ticket-product:title] added to your \u003Ca href=\u0022\/cart\u0022\u003Ecart\u003C\/a\u003E" } }
        ]
      }
    }';

  $rules['rules_add_ticket_products_to_cart_when_a_ticket_is_registered'] = entity_import('rules_config', $rule);

  // Import the rule that sets the ticket status when
  // an order is paid in full.
  $rule = '
    { "rules_update_ticket_status_when_order_is_paid_in_full" : {
        "LABEL" : "Update ticket status when order is paid in full",
        "PLUGIN" : "reaction rule",
        "OWNER" : "rules",
        "REQUIRES" : [ "commerce_ticket", "commerce_payment" ],
        "ON" : { "commerce_payment_order_paid_in_full" : [] },
        "DO" : [
          { "LOOP" : {
              "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
              "ITEM" : { "current_line_item" : "Current Line Item" },
              "DO" : [
                { "commerce_ticket_update_line_item_ticket_status" : { "line_item_wrapper" : [ "current-line-item" ], "state_wrapper" : "' . variable_get('commerce_ticket_status_complete', 2) . '" } }
              ]
            }
          }
        ]
      }
    }';

  $rules['rules_update_ticket_status_when_order_is_paid_in_full'] = entity_import('rules_config', $rule);

  // Create default rule to ad any selected product reference fields to the cart.
  $rule = '{ "commerce_ticket_ticket_registration_add_ons_add_to_cart" : {
      "LABEL" : "Add ticket add ons to cart when saving a new ticket registration",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "commerce_ticket", "commerce_cart", "ticket" ],
      "ON" : { "ticket_registration_insert" : [] },
      "DO" : [
        { "commerce_ticket_load_product_references_from_ticket" : {
            "USING" : { "ticket_registration" : [ "ticket_registration" ] },
            "PROVIDE" : { "products" : { "products" : "Products" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "products" ] },
            "ITEM" : { "current_product" : "Current product" },
            "DO" : [
              { "commerce_cart_product_add_by_sku" : {
                  "USING" : {
                    "user" : [ "site:current-user" ],
                    "sku" : [ "current-product:sku" ],
                    "quantity" : "1",
                    "combine" : "1"
                  },
                  "PROVIDE" : { "product_add_line_item" : { "line_item" : "Line Item" } }
                }
              }
            ]
          }
        }
      ]
    }
  }';

  $rules['commerce_ticket_ticket_registration_add_ons_add_to_cart']  = entity_import('rules_config', $rule);

  return $rules;
}
